import keras
import pandas as pd
import numpy as np
from keras import backend as K

from lib.TCN_Alex_V3 import TCN as tcn_v3
from lib.TCN_Alex_V4 import TCN as tcn_v4
from lib.WN_Fake import adam_wn, nadam_wn, AdamWithWeightnorm


# col_names of "one_minute_data"
col_names = ['time',
             'audusd_op', 'audusd_hi', 'audusd_lo', 'audusd_cl',
             'eurusd_op', 'eurusd_hi', 'eurusd_lo', 'eurusd_cl',
             'gbpusd_op', 'gbpusd_hi', 'gbpusd_lo', 'gbpusd_cl',
             'nzdusd_op', 'nzdusd_hi', 'nzdusd_lo', 'nzdusd_cl',
             'usdcad_op', 'usdcad_hi', 'usdcad_lo', 'usdcad_cl',
             'usdjpy_op', 'usdjpy_hi', 'usdjpy_lo', 'usdjpy_cl']

#might not be the same for different model, but for 20m_gbp they are all the same 
data_whole_list = ['time',
                   'audusd_op', 'audusd_hi', 'audusd_lo', 'audusd_cl',
                   'eurusd_op', 'eurusd_hi', 'eurusd_lo', 'eurusd_cl',
                   'gbpusd_op', 'gbpusd_hi', 'gbpusd_lo', 'gbpusd_cl',
                   'nzdusd_op', 'nzdusd_hi', 'nzdusd_lo', 'nzdusd_cl',
                   'usdcad_op', 'usdcad_hi', 'usdcad_lo', 'usdcad_cl',
                   'usdjpy_op', 'usdjpy_hi', 'usdjpy_lo', 'usdjpy_cl']

data_scale_list = ['audusd_op', 'audusd_hi', 'audusd_lo', 'audusd_cl',
                   'eurusd_op', 'eurusd_hi', 'eurusd_lo', 'eurusd_cl',
                   'gbpusd_op', 'gbpusd_hi', 'gbpusd_lo', 'gbpusd_cl',
                   'nzdusd_op', 'nzdusd_hi', 'nzdusd_lo', 'nzdusd_cl',
                   'usdcad_op', 'usdcad_hi', 'usdcad_lo', 'usdcad_cl',
                   'usdjpy_op', 'usdjpy_hi', 'usdjpy_lo', 'usdjpy_cl']


def recall(y_true, y_pred):
    return(K.constant(0))

def precision(y_true, y_pred):
    return(K.constant(0))


class model_pred():
    def __init__(self, model_name, hold, typ, threshold, lookback, start_time,
                 end_time, tcn_version, data_whole_list, data_scale_list):

        cos = {'TCN': tcn_v3 if (tcn_version==3) else tcn_v4,
               'precision': precision, 'recall': recall,
               'adam_wn':adam_wn, 'nadam_wn':nadam_wn, 'AdamWithWeightnorm':AdamWithWeightnorm}

        fname = model_name + '.hdf5'
        with keras.utils.CustomObjectScope(cos):
            self.model = keras.models.load_model(fname)

        self._mean = np.loadtxt(model_name + '_mean.txt')
        self._std = np.loadtxt(model_name + '_std.txt')
        self.typ = typ
        self.threshold = threshold
        self.lookback = lookback
        self.start_time = start_time
        self.end_time = end_time
        self.data_whole_list = data_whole_list
        self.data_scale_list = data_scale_list
        self.data = pd.DataFrame(columns=col_names, dtype=float)
        
        
    def predict(self, time, one_minute_data):
        
        self.data = self.data.append(pd.DataFrame(one_minute_data, columns=col_names, index=[0]),
                                     ignore_index=True)
        
        data_length = len(self.data)
        
        if data_length < self.lookback or time < self.start_time or time >= self.end_time:
            return 0, 0
        
        data_x_whole = np.array(self.data[self.data_whole_list])
        data_x_scale = np.array(self.data[self.data_scale_list])
        
        data_x_whole = data_x_whole[(data_length-self.lookback):data_length,:]
        data_x_scale = data_x_scale[(data_length-self.lookback):data_length,:]    
           
        #whole
        for i in range(data_x_whole.shape[1]):
            col_x = data_x_whole[:,i].copy()
            col_x[np.isnan(col_x)] = self._mean[i]
            col_x = (col_x - self._mean[i]) / self._std[i]
            data_x_whole[:,i] = col_x.copy()
        
        #scale
        for i in range(data_x_scale.shape[1]):
            if np.isnan(data_x_scale[:,i]).all() :
                data_x_scale[:,i] = np.zeros_like(data_x_scale[:,i])
    
        w_mean = np.nanmean(data_x_scale, axis=0)
        w_std = np.nanstd(data_x_scale, axis=0)        
        
        for i in range(data_x_scale.shape[1]):
            col_x = data_x_scale[:,i].copy()
            if w_std[i] == 0:
                col_x = np.zeros_like(col_x)
            else:
                col_x[np.isnan(col_x)] = w_mean[i]
                col_x = (col_x - w_mean[i]) / w_std[i]
                data_x_scale[:,i] = col_x.copy()
        
        #all
        all_x = np.concatenate((data_x_whole, data_x_scale), axis=1)
        all_x = np.expand_dims(all_x, axis=0)
        
        one_pred = self.model.predict_on_batch(all_x)
        
        signal = float(one_pred)
        if signal > self.threshold:
            if self.typ=='long':
                doit = 1
            else :
                doit = -1
        else:
            doit = 0
        
        return doit, signal


if __name__ == '__main__':
    
    '''testing'''
    
    m1 = model_pred(model_name = '1791 0.5694 0.0385 - 940 0.6178 0.0242 - 0.0652', 
                    hold = 20,
                    typ = 'long', 
                    threshold = 0.0652, 
                    lookback = 120, 
                    start_time = 720,
                    end_time = 960,
                    tcn_version = 3,
                    data_whole_list = data_whole_list, 
                    data_scale_list = data_scale_list)
    
    data_x = pd.read_csv('x_20190222.csv')
    data_x = data_x[col_names]
    result = pd.DataFrame(data_x['time'])
    
    for i in range(data_x.shape[0]):
        time = data_x.loc[i,'time']
        one_minute_data = data_x.iloc[i,].to_dict()
        result.loc[i,'doit'], result.loc[i,'pred'] = m1.predict(time, one_minute_data)
