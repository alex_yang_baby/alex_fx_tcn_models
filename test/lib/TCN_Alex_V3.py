import keras.layers
from keras.layers import Activation, Lambda
from keras.layers import Conv1D, SpatialDropout1D


def residual_block(x, dilation, 
                   nb_filters, kernel_size, stacks_per_block,
                   padding, activation,
                   dropout_rate, w_maxnorm, w_l2):
    
    # main reason using the residual block is to do the mapping,
    # by adding the original input at the end of the block will effectively protect the gradient,
    # hence its possible to train extremely deep NN without any degradation (He et al., 2016)
    
    # stacks_per_block controls how many conv net work are gonna stack in one block,
    # in the work "An Empirical Evaluation of Generic Convolutional and Recurrent Networks for Sequence Modeling"
    # they used 2 conv to construct a residual block
    
    original_x = x
    
    for i in range(stacks_per_block):
        
        if activation=="gated" or activation=="linear_gated":
            temp_nb_filters = nb_filters * 2
        else:
            temp_nb_filters = nb_filters
            
        x = Conv1D(filters=temp_nb_filters,
                   kernel_size=kernel_size,
                   dilation_rate=dilation,
                   padding=padding,
                   kernel_constraint = keras.constraints.MaxNorm(w_maxnorm),
                   kernel_regularizer = keras.regularizers.l2(w_l2),
                   name = 'DilatedConv_d%ds%d' % (dilation,(i+1)))(x)  
        
        if activation=="gated":
            f = Lambda(lambda tt: tt[:,:,:nb_filters])(x)
            g = Lambda(lambda tt: tt[:,:,nb_filters:])(x)
            f = Activation('tanh')(f) 
            g = Activation('sigmoid')(g)
            x = keras.layers.multiply([f, g])
            
        elif activation=="linear_gated":
            f = Lambda(lambda tt: tt[:,:,:nb_filters])(x)
            g = Lambda(lambda tt: tt[:,:,nb_filters:])(x)            
            g = Activation('sigmoid')(g)
            x = keras.layers.multiply([f, g])

        elif activation=="simple_gated":
            f = Activation('tanh')(x) 
            g = Activation('sigmoid')(x)
            x = keras.layers.multiply([f, g])

        elif activation=="simple_linear_gated":
            f = x
            g = Activation('sigmoid')(x)
            x = keras.layers.multiply([f, g])
            
        else :           
            x = Activation(activation)(x)
        
        x = SpatialDropout1D(dropout_rate)(x)
    
    # identity mapping is the best residual training approach (He et al., 2016)
    # which means no operation on the original x would be better

    final_x = keras.layers.add([original_x, x])
    
    return final_x, x
     
    
    

class TCN:

    def __init__(self,
                 nb_filters=100,
                 kernel_size=5,
                 stacks_per_block=1,
                 dilations=[1,2,4,8,16,32],
                 padding='casual',
                 activation='gated',
                 dropout_rate=0.2,
                 w_maxnorm=50,
                 w_l2=0,
                 skip_connection=False,
                 return_sequences=False):
        self.nb_filters = nb_filters
        self.kernel_size = kernel_size
        self.stacks_per_block = stacks_per_block
        self.dilations = dilations
        self.padding = padding
        self.activation = activation
        self.dropout_rate = dropout_rate
        self.w_maxnorm = w_maxnorm
        self.w_l2 = w_l2
        self.skip_connection = skip_connection
        self.return_sequences = return_sequences
        
      

    def __call__(self, inputs):
        x = inputs
        if self.skip_connection:
            sc = []
        for i in self.dilations:
            x, skip_out = residual_block(x, i,
                                         self.nb_filters, self.kernel_size, self.stacks_per_block,
                                         self.padding, self.activation,
                                         self.dropout_rate, self.w_maxnorm, self.w_l2)
            if self.skip_connection:
                sc.append(skip_out)
            
        if self.skip_connection:
            x = keras.layers.add(sc)
            
        if not self.return_sequences:
            x = Lambda(lambda tt: tt[:, -1, :])(x)
            
        return x
